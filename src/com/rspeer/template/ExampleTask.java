package com.rspeer.template;

import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

/**
 * Created by Cam on 5/11/2018.
 */

public class ExampleTask extends Task {

    @Override
    public boolean validate() {
        return Inventory.contains("Coins"); // Boolean to check before running the task. If successful, it will run this task, if it fails, it'll skip this task and go on to the next task in the order submitted in onStart();
    }

    @Override
    public int execute() {
        Log.info("Poggers i've got coins");
        return 100;
    }

}