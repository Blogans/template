package com.rspeer.template;

import org.rspeer.runetek.event.listeners.ChatMessageListener;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.ChatMessageEvent;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.TaskScript;
import org.rspeer.ui.Log;

@ScriptMeta(developer = "Apache", category = ScriptCategory.COMBAT, desc = "Public Revenants", name = "Public Revenants",version = 0.01)

public class Main extends TaskScript implements RenderListener, ChatMessageListener{

    @Override
    public void onStart(){
        Log.info("Script starting");
        submit(new ExampleTask());
    }

    @Override
    public void onStop(){
        Log.info("Script stopping");
    }

    @Override
    public void notify(RenderEvent ev){
        //Used for rendering stuff on screen
    }

    @Override
    public void notify(ChatMessageEvent ev){
        //Used to check chat messages
    }

}
